import express from "express";

const app = express();
const port = 3000;

app.get("/", (_, res) => {
  res.send("OK!");
});

app.get("/status", (_, res) => {
  res.status(200).send();
});

app.get("/city", function (_, res) {
  const city = [
    "Paris",
    "Bordeaux",
    "Lyon",
    "Strasbourg",
    "Toulouse",
    "Marseille",
  ];
  res.json(city);
});

app.use(function (_, res) {
  res.status(404).send("Not Found");
});

app.listen(port, () => {
  console.log(`Listening on port ${port}...`);
});
