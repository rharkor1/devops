CREATE TABLE
    IF NOT EXISTS city (
        id INT AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(255) NOT NULL
    );

DELETE FROM `city`;

INSERT INTO
    `city` (`name`)
VALUES
    ('New York');

INSERT INTO
    `city` (`name`)
VALUES
    ('Los Angeles');

INSERT INTO
    `city` (`name`)
VALUES
    ('Chicago');

INSERT INTO
    `city` (`name`)
VALUES
    ('Houston');