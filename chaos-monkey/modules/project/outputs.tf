output "instance_ip" {
  value = aws_instance.ec2.public_ip
}

output "ssh_key_path" {
  value = "${path.module}/keys/${var.project_name}_key"
}

output "project_name" {
  value = var.project_name
}