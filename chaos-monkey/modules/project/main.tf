variable "project_name" {
  type = string
}

resource "aws_key_pair" "ssh_key" {
  key_name   = "${var.project_name}_key"
  public_key = file("${path.module}/keys/${var.project_name}_key.pub")
}

resource "aws_security_group" "sg" {
  name        = "${var.project_name}-sg"
  description = "Allow port 80"

  dynamic "ingress" {
    for_each = [
      { from_port = 80, to_port = 80, protocol = "tcp", cidr_blocks = ["0.0.0.0/0"] },
      { from_port = 22, to_port = 22, protocol = "tcp", cidr_blocks = ["0.0.0.0/0"] },
      { from_port = -1, to_port = -1, protocol = "icmp", cidr_blocks = ["0.0.0.0/0"] }
    ]

    content {
      from_port   = ingress.value.from_port
      to_port     = ingress.value.to_port
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.cidr_blocks
    }
  }

  dynamic "egress" {
    for_each = [
      { from_port = 0, to_port = 0, protocol = "-1", cidr_blocks = ["0.0.0.0/0"] },
    ]

    content {
      from_port   = egress.value.from_port
      to_port     = egress.value.to_port
      protocol    = egress.value.protocol
      cidr_blocks = egress.value.cidr_blocks
    }
  }
}

resource "aws_instance" "ec2" {
  ami           = "ami-01d21b7be69801c2f"
  instance_type = "t2.micro"
  key_name      = aws_key_pair.ssh_key.key_name
  security_groups = [aws_security_group.sg.name]

  tags = {
    Name = var.project_name
  }
}