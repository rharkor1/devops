import * as fs from "fs/promises";

const main = async () => {
  const data = await fs.readFile("./output.json", "utf-8");
  const json = JSON.parse(data);
  const projects = json.project_details.value as {
    [key: string]: { ip: string; ssh_key_path: string };
  };
  const ips = Object.values(projects);

  // Save to inventory file
  await fs.writeFile(
    "./ansible/inventory.ini",
    `[nodes]
${ips
  .map(
    (v) =>
      `${v.ip} ansible_user=ubuntu ansible_ssh_private_key_file=${v.ssh_key_path}\n`
  )
  .join("")}
`,
    "utf-8"
  );
};

main();
