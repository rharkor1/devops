output "project_details" {
  value = { for project in module.project : project.project_name => { ip = project.instance_ip, ssh_key_path = project.ssh_key_path } }
}
