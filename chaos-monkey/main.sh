#!/bin/sh

# Apply terraform
echo "Apply terraform"
terraform apply -auto-approve
echo "Terraform applied"
# Save output
echo "Save terraform output"
terraform output -json >output.json
echo "Terraform output saved"
# Set all ip in ansible/inventory.ini
echo "Set all ip in ansible/inventory.ini"
npx tsx inventory.ts
echo "All ip set in ansible/inventory.ini"
# Run ansible playbook
echo "Run ansible playbook"
sleep 10 # wait for ssh to be ready (sleep for 10 seconds)
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i ansible/inventory.ini ansible/playbook.yml
echo "Ansible playbook run"
