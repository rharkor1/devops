terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region = "eu-west-3"
}

module "project" {
  source = "./modules/project"
  for_each = toset(["project1", "project2", "project3", "project4", "project5", "project6", "project7", "project8", "project9", "project10", "project11"])

  project_name = each.value
}