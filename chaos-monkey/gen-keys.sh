#!/bin/bash

# Generate private keys for the Chaos Monkey
# names: project1, project2
# directory: modules/project/keys
# files: project_name_key, project_name_key.pub
for i in {1..11}; do
    ssh-keygen -t rsa -C "project${i}_key" -f "modules/project/keys/project${i}_key" -N "" <<<y >/dev/null 2>&1
    echo "project$i key generated"
done
