<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hello</title>
</head>

<body>
    <h1>Hello</h1>
    <h2>List of cities:</h2>
    <?php
    require('mysql.php');
    $req = $bdd->prepare('SELECT name FROM city');
    $req->execute();

    $result = $req->fetchAll(PDO::FETCH_ASSOC);
    foreach ($result as $city) {
        echo '<p>' . $city['name'] . '</p>';
    }

    ?>
</body>

</html>